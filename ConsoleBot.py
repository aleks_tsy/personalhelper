from AbstractBot import AbstractBot
from AddressBook import *


class ConsoleBot(AbstractBot):
    def __init__(self):
        self.book = AddressBook()

    def _add_handle(self):
        name = Name(input("Name: ")).value.strip()
        phones = Phone().value
        birth = Birthday().value
        email = Email().value.strip()
        status = Status().value.strip()
        note = Note(input("Note: ")).value
        record = Record(name, phones, birth, email, status, note)
        return record

    def _search_handle(self):
        print("There are following categories: \nName \nPhones \nBirthday \nEmail \nStatus \nNote")
        category = input('Search category: ')
        pattern = input('Search pattern: ')
        result = (self.book.search(pattern, category))
        for account in result:
            if account['birthday']:
                birth = account['birthday'].strftime("%d/%m/%Y")
                result = "_" * 50 + "\n" + f"Name: {account['name']} \nPhones: {', '.join(account['phones'])} \nBirthday: {birth} \nEmail: {account['email']} \nStatus: {account['status']} \nNote: {account['note']}\n" + "_" * 50
                print(result)

    def _edit_handle(self):
        contact_name = input('Contact name: ')
        parameter = input('Which parameter to edit(name, phones, birthday, status, email, note): ').strip()
        new_value = input("New Value: ")
        return self.book.edit(contact_name, parameter, new_value)

    def _remove_handle(self):
        pattern = input("Remove (contact name or phone): ")
        return self.book.remove(pattern)

    def _save_handle(self):
        file_name = input("File name: ")
        return self.book.save(file_name)

    def _load_handle(self):
        file_name = input("File name: ")
        return self.book.load(file_name)

    def _congratulate_handle(self):
        print(self.book.congratulate())

    def _view_handle(self):
        print(self.book)

    def _exit_handle(self):
        pass

    def handle(self, action):
        if action == 'add':
            return self.book.add(self._add_handler())
        elif action == 'search':
            self._search_handler()
        elif action == 'edit':
            return self._edit_handler()
        elif action == 'remove':
            return self._remove_handle()
        elif action == 'save':
            return self._save_handle()
        elif action == 'load':
            return self._load_handle()
        elif action == 'congratulate':
            self._congratulate_handle()
        elif action == 'view':
            self._view_handle()
        elif action == 'exit':
            pass
        else:
            print("There is no such command!")
