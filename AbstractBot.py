from abc import ABC, abstractmethod


class AbstractBot(ABC):
    @abstractmethod
    def handle(self):
        pass

    def _edit_handle(self):
        pass

    def _remove_handle(self):
        pass

    def _save_handle(self):
        pass

    def _load_handle(self):
        pass

    def _congratulate_handle(self):
        pass

    def _view_handle(self):
        pass

    def _exit_handle(self):
        pass
